# README #

Даний репозиторій призначений для студентів ВНТУ, які відвідують курси PHP, Yii2.
У репозиторыъ знаходяться лише матеріали та завдання.

[Course Wiki](https://bitbucket.org/dev_skoval/vntu_php_course/wiki/Home)

*Wiki буде оновлюватися починаючи із теми БД*

### How do I get set up? ###

Для завантаження вмісту репозитарія потрібно встановити GIT. 
https://git-scm.com/

У терміналі створити власну директорію, та запустити наступну команду: 
git clone https://dev_skoval@bitbucket.org/dev_skoval/vntu_php_course.git