#### База даних MySQL course_db. ####

**Для встановлення бази даних імпортуйте файл - mysql_course_db.sql у вашу (створену) базу даних.**

*Схема бази даних MySQL складається з наступних таблиць:*

1. Customers: сховища даних клієнта.

2. Products: зберігає список моделей автомобілів.

3. ProductLines: зберігає список категорій продуктів.

4. Orders: замовлення магазинів продажів,які розміщені клієнтами.

5. OrderDetails: позиція для кожного замовлення клієнта.

6. Payments: платежі, зроблені клієнтами, засновані на їх рахунках.

7. Employees: зберігає всю інформацію співробітників, а також організаційну структуру.

8. Offices: магазини офіс продажів даних.

**У diagram.png знаходиться діаграма залежності ключів усіх таблиць БД course_db**
